package com.emergency.service.user;

import com.emergency.domain.user.User;
import com.emergency.domain.user.UserRoleType;
import com.emergency.repository.user.UserRepository;
import com.emergency.repository.user.UserRoleRepository;
import com.emergency.service.user.dto.UserDTO;
import com.emergency.service.user.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    public User createUser(UserDTO userDTO) {
        User newUser = UserMapper.dtoToDomain(userDTO);
        UserRoleType userRoleType = UserRoleType.valueOf(userDTO.getRole());
        newUser.setRole(userRoleRepository.findDistinctByUserRoleType(userRoleType));
        newUser.setPassword(generatePassword());
        return userRepository.save(newUser);
    }

    public String generatePassword(){
        char[] chars = "ABCDEFGHIJKLMNOPRSTUWXYZabcdefghijklmnopqrstuvwxyz/[]{}:;'123456789".toCharArray();
        StringBuilder sb = new StringBuilder(10);
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }

        return sb.toString();
    }

}
