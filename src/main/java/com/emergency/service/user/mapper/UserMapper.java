package com.emergency.service.user.mapper;

import com.emergency.domain.user.User;
import com.emergency.domain.user.UserRole;
import com.emergency.domain.user.UserRoleType;
import com.emergency.repository.user.UserRoleRepository;
import com.emergency.service.user.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;

public class UserMapper {

    public static User dtoToDomain(UserDTO userDTO) {
        User user = User.builder()
                .id(0L)
                .username(userDTO.getUsername())
                .name(userDTO.getName())
                .surname(userDTO.getSurname())
                .email(userDTO.getEmail())
                .build();
        return user;
    }
}
