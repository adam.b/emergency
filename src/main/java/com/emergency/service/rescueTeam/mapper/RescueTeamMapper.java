package com.emergency.service.rescueTeam.mapper;

import com.emergency.domain.rescueAction.RescueAction;
import com.emergency.domain.rescueAction.RescueActionType;
import com.emergency.domain.rescueTeam.RescueTeam;
import com.emergency.service.rescueTeam.dto.RescueTeamDTO;

import java.time.LocalDateTime;

public class RescueTeamMapper {
    public static RescueTeamDTO domainToDTO(RescueTeam rescueTeam) {

        return RescueTeamDTO.builder()
                .id(rescueTeam.getId())
                .status(rescueTeam.getRescueTeamStatus().getStatus())
                .build();
    }
}
