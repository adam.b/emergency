package com.emergency.service.rescueTeam.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class RescueTeamDTO {

    private Long id;
    private String type;
    private String status;
}
