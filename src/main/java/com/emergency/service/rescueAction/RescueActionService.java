package com.emergency.service.rescueAction;

import com.emergency.domain.rescueAction.RescueAction;
import com.emergency.domain.rescueAction.RescueActionType;
import com.emergency.domain.rescueTeam.RescueTeam;
import com.emergency.domain.rescueTeam.RescueTeamStatus;
import com.emergency.domain.user.User;
import com.emergency.repository.rescueAction.RescueActionRepository;
import com.emergency.repository.rescueTeam.RescueTeamRepository;
import com.emergency.repository.user.UserRepository;
import com.emergency.service.rescueAction.dto.RescueActionDTO;
import com.emergency.service.rescueAction.mapper.RescueActionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class RescueActionService {

    @Autowired
    private RescueActionRepository rescueActionRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RescueTeamRepository rescueTeamRepository;

    public RescueAction createRescueAction(RescueActionDTO rescueActionDTO){

        System.out.println("create rescue action");

        for(Long team: rescueActionDTO.getRescueTeamList()){
            Optional<RescueTeam> rescueTeam = rescueTeamRepository.findById(team);
            RescueTeam newRescueTeam = rescueTeam.get();
            newRescueTeam.setRescueTeamStatus(RescueTeamStatus.ON_THE_WAY);
            rescueTeamRepository.save(newRescueTeam);

        }

        return rescueActionRepository.save(RescueActionMapper.dtoToDomain(rescueActionDTO));
    }

    public RescueAction getRescueActionByUsername(String username){
        RescueAction rescueAction;

        User user = userRepository.findByUsername(username);

        //System.out.println("USER: " + user.getUsername());

        RescueTeam rescueTeam = rescueTeamRepository.findFirstByRescuersAndDate(user.getId(), LocalDate.now());

        //System.out.println("TEAM: " + rescueTeam.getRescuers());

        rescueAction = rescueActionRepository.findByRescueTeamsAndActiveTrue(rescueTeam.getId());



        return rescueAction;
    }

    public List<RescueAction> getActiveRescueActions(){
        return rescueActionRepository.findAllByActiveTrue();
    }
}
