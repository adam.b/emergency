package com.emergency.service.rescueAction.dto;

import com.emergency.domain.rescueAction.RescueActionType;
import com.emergency.domain.rescueTeam.RescueTeam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class RescueActionDTO {

    private Long id;
    private String actionType;
    private Long numberOfVictims;
    private List<Long> rescueTeamList;
    private String address;
    private String injuries;
    

}
