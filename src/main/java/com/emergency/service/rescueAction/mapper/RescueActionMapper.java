package com.emergency.service.rescueAction.mapper;

import com.emergency.domain.rescueAction.RescueAction;
import com.emergency.domain.rescueAction.RescueActionType;
import com.emergency.service.geocode.GeocodeEnum;
import com.emergency.service.geocode.GeocodeService;
import com.emergency.service.rescueAction.dto.RescueActionDTO;
import org.primefaces.model.map.LatLng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Collection;

public class RescueActionMapper {

    public static RescueAction dtoToDomain(RescueActionDTO rescueActionDTO){

        Double longitude = GeocodeService.handleGeocode(rescueActionDTO.getAddress()).get(GeocodeEnum.LONGITUDE);
        Double latitude = GeocodeService.handleGeocode(rescueActionDTO.getAddress()).get(GeocodeEnum.LATITUDE);
        LatLng latLng = new LatLng(latitude, longitude);

        return RescueAction.builder()
                .rescueActionType(RescueActionType.getInstance(rescueActionDTO.getActionType()))
                .time(LocalDateTime.now())
                .address(rescueActionDTO.getAddress())
                .numberOfVictims(rescueActionDTO.getNumberOfVictims())
                .rescueTeams(rescueActionDTO.getRescueTeamList())
                .injuries(rescueActionDTO.getInjuries())
                .coordinates(latLng)
                .active(true)
                .build();
    }
}
