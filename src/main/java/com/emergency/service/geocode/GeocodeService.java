package com.emergency.service.geocode;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.java.Log;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Log
@Service
public class GeocodeService implements Serializable {


    public static Map<GeocodeEnum, Double> handleGeocode(String address) {
        ResteasyClient client = new ResteasyClientBuilder().build();
        Response response = sendRequest(client, address);
        return handleResponse(response);
    }

    private static Response sendRequest(Client client, String address) {
        address = address.replace(" ", "+");
        return client.target("https://maps.googleapis.com/maps/api/geocode/json?address=" + address +
                "&key=AIzaSyDPaP4zG0IKpItktSLaWYRSs9oKqTc7O3o")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
    }

    private static Map<GeocodeEnum, Double> handleResponse(Response response) {
        switch (response.getStatus()) {

            case HttpURLConnection.HTTP_OK: {
                return readResponse(response);
            }
            default: {
                log.info("Błąd");
            }
        }
        return Collections.emptyMap();
    }

    private static Map<GeocodeEnum, Double> readResponse(Response response) {
        Map<GeocodeEnum, Double> geoMap = new HashMap<>();
        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(response.readEntity(String.class)).getAsJsonObject();
        JsonArray jsonArray = json.get("results").getAsJsonArray();
        JsonObject results = jsonArray.get(0).getAsJsonObject();
        JsonObject geometry = results.get("geometry").getAsJsonObject();
        JsonObject location = geometry.get("location").getAsJsonObject();
        log.info("LATITUDE:" + location.get("lat").getAsString() + " , LONGITUDE:" + location.get("lng").getAsString());
        geoMap.put(GeocodeEnum.LATITUDE, Double.valueOf(location.get("lat").getAsString()));
        geoMap.put(GeocodeEnum.LONGITUDE, Double.valueOf(location.get("lng").getAsString()));
        return geoMap;
    }


    public static void main(String[] args) {
        handleGeocode("Warszawska 99 Kielce");
    }


}


