package com.emergency.service;

import com.emergency.domain.ambulance.Ambulance;
import com.emergency.repository.ambulance.AmbulanceRepository;
import com.emergency.service.ambulance.dto.AmbulanceDTO;
import com.emergency.service.ambulance.mapper.AmbulanceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AmbulanceService {

    @Autowired
    private AmbulanceRepository ambulanceRepository;

    public Ambulance createAmbulance(AmbulanceDTO ambulanceDTO){
        return ambulanceRepository.save(AmbulanceMapper.dtoToDomain(ambulanceDTO));
    }

}
