package com.emergency.service.ambulance.mapper;

import com.emergency.domain.ambulance.Ambulance;
import com.emergency.domain.ambulance.AmbulanceType;
import com.emergency.service.ambulance.dto.AmbulanceDTO;

public class AmbulanceMapper {

    public static Ambulance dtoToDomain(AmbulanceDTO ambulanceDto) {

        Ambulance ambulance = Ambulance.builder()
                .id(0L)
                .licensePlate(ambulanceDto.getLicensePlate())
                .ambulanceType(AmbulanceType.getInstance(ambulanceDto.getAmbulanceType()))
                .build();

        return ambulance;
    }
}
