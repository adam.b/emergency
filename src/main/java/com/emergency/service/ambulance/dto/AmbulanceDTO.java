package com.emergency.service.ambulance.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class AmbulanceDTO {

    private Long id;
    private String licensePlate;
    private String ambulanceType;
}
