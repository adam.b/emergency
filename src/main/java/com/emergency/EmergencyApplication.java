package com.emergency;

import lombok.extern.java.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Log
@SpringBootApplication
public class EmergencyApplication {


    public static void main(String[] args) {
        SpringApplication.run(EmergencyApplication.class, args);
        System.out.println("Application initialized");
    }

