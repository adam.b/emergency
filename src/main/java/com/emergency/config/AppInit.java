package com.emergency.config;

import com.emergency.domain.ambulance.Ambulance;
import com.emergency.domain.ambulance.AmbulanceType;
import com.emergency.domain.rescueAction.RescueAction;
import com.emergency.domain.rescueAction.RescueActionType;
import com.emergency.domain.rescueTeam.RescueTeam;
import com.emergency.domain.rescueTeam.RescueTeamStatus;
import com.emergency.domain.user.UserRole;
import com.emergency.domain.user.UserRoleType;
import com.emergency.domain.user.User;
import com.emergency.repository.ambulance.AmbulanceRepository;
import com.emergency.repository.rescueAction.RescueActionRepository;
import com.emergency.repository.rescueTeam.RescueTeamRepository;
import com.emergency.repository.user.UserRepository;
import com.emergency.repository.user.UserRoleRepository;
import com.emergency.service.geocode.GeocodeEnum;
import com.emergency.service.geocode.GeocodeService;
import lombok.extern.java.Log;
import org.primefaces.model.map.LatLng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
@Log
@Component
public class AppInit {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private AmbulanceRepository ambulanceRepository;

    @Autowired
    private RescueTeamRepository rescueTeamRepository;

    @Autowired
    private RescueActionRepository rescueActionRepository;


    private Ambulance specialistAmbulance, basicAmbulance, transportAmbulance;
    private User user, doctor, doctor2, paramedic1, paramedic2, paramedic3, paramedic4, paramedic5, paramedic6;
    private RescueTeam rescueTeam;

    @PostConstruct
    void initialize() {
        initUserRoles();
        initUsers();
        initAmbulances();
        initRescueTeams();
        initRescueActions();

        /*User user = new User("user", ("user"), Arrays.asList(new UserRoleType("ADMIN")));
        userRepository.save(user);
        User user1 = new User("user1", ("user1"), Arrays.asList(new UserRoleType("USER")));
        userRepository.save(user1);*/
    }

    private void initUserRoles() {
        Collection<UserRole> roles = Arrays.asList(
                new UserRole(0L, UserRoleType.ROLE_ADMIN),
                new UserRole(0L, UserRoleType.ROLE_DOCTOR),
                new UserRole(0L, UserRoleType.ROLE_DISPATCHER),
                new UserRole(0L, UserRoleType.ROLE_RESCUER));

        userRoleRepository.saveAll(roles);
    }

    private void initUsers() {

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

        User admin;
        admin = User.builder()
                .id(0L)
                .username("admin")
                .password(bCryptPasswordEncoder.encode("admin"))
                .name("Zdzisław")
                .surname("Kulis")
                .role(userRoleRepository.findDistinctByUserRoleType(UserRoleType.ROLE_ADMIN))
                .active(true)
                .build();

        userRepository.save(admin);

        user = User.builder()
                .id(0L)
                .username("user")
                .password(bCryptPasswordEncoder.encode("user"))
                .name("Jan")
                .surname("Nowak")
                .role(userRoleRepository.findDistinctByUserRoleType(UserRoleType.ROLE_DOCTOR))
                .active(true)
                .build();

        userRepository.save(user);

        User dispatcher = User.builder()
                .id(0L)
                .username("dispatcher")
                .password(bCryptPasswordEncoder.encode("dispatcher"))
                .name("Maciej")
                .surname("Kowalski")
                .role(userRoleRepository.findDistinctByUserRoleType(UserRoleType.ROLE_DISPATCHER))
                .active(true)
                .build();

        userRepository.save(dispatcher);

        doctor = User.builder()
                .id(0L)
                .username("doctor")
                .password(bCryptPasswordEncoder.encode("doctor"))
                .name("Jan")
                .surname("Zimoch")
                .role(userRoleRepository.findDistinctByUserRoleType(UserRoleType.ROLE_RESCUER))
                .active(true)
                .build();

        userRepository.save(doctor);

        doctor2 = User.builder()
                .id(0L)
                .username("doctor2")
                .password(bCryptPasswordEncoder.encode("doctor"))
                .name("Jan")
                .surname("Zimoch")
                .role(userRoleRepository.findDistinctByUserRoleType(UserRoleType.ROLE_RESCUER))
                .active(true)
                .build();

        userRepository.save(doctor2);

        paramedic1 = User.builder()
                .id(0L)
                .username("paramedic1")
                .password(bCryptPasswordEncoder.encode("paramedic"))
                .name("Szymon")
                .surname("Szyba")
                .role(userRoleRepository.findDistinctByUserRoleType(UserRoleType.ROLE_RESCUER))
                .active(true)
                .build();

        userRepository.save(paramedic1);

        paramedic2 = User.builder()
                .id(0L)
                .username("paramedic2")
                .password(bCryptPasswordEncoder.encode("paramedic"))
                .name("Wojciech")
                .surname("Kubicki")
                .role(userRoleRepository.findDistinctByUserRoleType(UserRoleType.ROLE_RESCUER))
                .active(true)
                .build();

        userRepository.save(paramedic2);

        paramedic3 = User.builder()
                .id(0L)
                .username("paramedic3")
                .password(bCryptPasswordEncoder.encode("paramedic"))
                .name("Zbigniewh")
                .surname("Kowalski")
                .role(userRoleRepository.findDistinctByUserRoleType(UserRoleType.ROLE_RESCUER))
                .active(true)
                .build();

        userRepository.save(paramedic3);

        paramedic4 = User.builder()
                .id(0L)
                .username("paramedic4")
                .password(bCryptPasswordEncoder.encode("paramedic"))
                .name("Karol")
                .surname("Nowicki")
                .role(userRoleRepository.findDistinctByUserRoleType(UserRoleType.ROLE_RESCUER))
                .active(true)
                .build();

        userRepository.save(paramedic4);

        paramedic5 = User.builder()
                .id(0L)
                .username("paramedic5")
                .password(bCryptPasswordEncoder.encode("paramedic"))
                .name("Zbigniewh")
                .surname("Kowalski")
                .role(userRoleRepository.findDistinctByUserRoleType(UserRoleType.ROLE_RESCUER))
                .active(true)
                .build();

        userRepository.save(paramedic5);

        paramedic6 = User.builder()
                .id(0L)
                .username("paramedic6")
                .password(bCryptPasswordEncoder.encode("paramedic"))
                .name("Karol")
                .surname("Nowicki")
                .role(userRoleRepository.findDistinctByUserRoleType(UserRoleType.ROLE_RESCUER))
                .active(true)
                .build();

        userRepository.save(paramedic6);
    }

    private void initAmbulances(){
        basicAmbulance = Ambulance.builder()
                .id(0L)
                .licensePlate("TK09876")
                .ambulanceType(AmbulanceType.BASIC)
                .build();

        ambulanceRepository.save(basicAmbulance);

        specialistAmbulance = Ambulance.builder()
                .id(0L)
                .licensePlate("TK23145")
                .ambulanceType(AmbulanceType.SPECIALIST)
                .build();

        ambulanceRepository.save(specialistAmbulance);

        transportAmbulance = Ambulance.builder()
                .id(0L)
                .licensePlate("TK65437")
                .ambulanceType(AmbulanceType.TRANSPORT)
                .build();

        ambulanceRepository.save(transportAmbulance);
    }

    private void initRescueTeams(){

        Double lng = GeocodeService.handleGeocode("Kielce, Padarewskiego 11").get(GeocodeEnum.LONGITUDE);
        Double lat = GeocodeService.handleGeocode("Kielce, Paderewskiego 11").get(GeocodeEnum.LATITUDE);
        LatLng latLng1 = new LatLng(lat,lng);

        Double lng2 = GeocodeService.handleGeocode("Politechnika Swietokrzyska").get(GeocodeEnum.LONGITUDE);
        Double lat2 = GeocodeService.handleGeocode("Politechnika Swietokrzyska").get(GeocodeEnum.LATITUDE);
        LatLng latLng2 = new LatLng(lat2,lng2);

        Double lng3 = GeocodeService.handleGeocode("Galeria Korona kielce").get(GeocodeEnum.LONGITUDE);
        Double lat3 = GeocodeService.handleGeocode("Galeria Korona kielce").get(GeocodeEnum.LATITUDE);
        LatLng latLng3 = new LatLng(lat3,lng3);

        List<Long> rescuersList = new ArrayList<>();
        rescuersList.add(userRepository.findByUsername("user").getId());
        rescuersList.add(userRepository.findByUsername("paramedic1").getId());
        rescuersList.add(userRepository.findByUsername("paramedic2").getId());
        System.out.println("RATOWNICY "+rescuersList);

        List<Long> rescuersList1 = new ArrayList<>();
        rescuersList1.add(userRepository.findByUsername("doctor").getId());
        rescuersList1.add(userRepository.findByUsername("paramedic3").getId());
        rescuersList1.add(userRepository.findByUsername("paramedic4").getId());
        System.out.println("RATOWNICY "+rescuersList1);

        List<Long> rescuersList2 = new ArrayList<>();
        rescuersList2.add(userRepository.findByUsername("doctor2").getId());
        rescuersList2.add(userRepository.findByUsername("paramedic5").getId());
        rescuersList2.add(userRepository.findByUsername("paramedic6").getId());
        System.out.println("RATOWNICY "+rescuersList2);

        rescueTeam = RescueTeam.builder()
                .id(0L)
                .ambulance(ambulanceRepository.findByLicensePlate("TK23145").getId())
                .rescueTeamStatus(RescueTeamStatus.ON_ACTION)
                .rescuers(rescuersList1)
                .date(LocalDate.now())
                .coordinates(latLng1)
                .build();

        rescueTeamRepository.save(rescueTeam);

        RescueTeam rescueTeam1 = RescueTeam.builder()
                .id(0L)
                .ambulance(ambulanceRepository.findByLicensePlate("TK09876").getId())
                .rescueTeamStatus(RescueTeamStatus.READY)
                .rescuers(rescuersList)
                .date(LocalDate.now())
                .coordinates(latLng2)
                .build();

        rescueTeamRepository.save(rescueTeam1);

        RescueTeam rescueTeam2 = RescueTeam.builder()
                .id(0L)
                .ambulance(ambulanceRepository.findByLicensePlate("TK65437").getId())
                .rescueTeamStatus(RescueTeamStatus.TRANSPORT)
                .rescuers(rescuersList2)
                .date(LocalDate.now())
                .coordinates(latLng3)
                .build();

        rescueTeamRepository.save(rescueTeam2);
    }

    private void initRescueActions(){
        Double lng = GeocodeService.handleGeocode("Kielce, Pocieszka 123").get(GeocodeEnum.LONGITUDE);
        Double lat = GeocodeService.handleGeocode("Kielce, Pocieszka 123").get(GeocodeEnum.LATITUDE);
        LatLng latLng = new LatLng(lat,lng);
        RescueAction rescueAction1 = RescueAction.builder()
                .id(0L)
                .rescueActionType(RescueActionType.ACCIDENT)
                .rescueTeams(Collections.singletonList(rescueTeamRepository.findFirstByRescueTeamStatus(RescueTeamStatus.ON_ACTION).getId()))
                .time(LocalDateTime.now())
                .numberOfVictims(1L)
                .coordinates(latLng)
                .injuries("Uraz głowy oraz kręgosłupa")
                .address("Kielce, Pocieszka 123")
                .active(true)
                .build();

        rescueActionRepository.save(rescueAction1);

        Double lng2 = GeocodeService.handleGeocode("Kielce, Warszawska 34").get(GeocodeEnum.LONGITUDE);
        Double lat2 = GeocodeService.handleGeocode("Kielce, Warszawska 34").get(GeocodeEnum.LATITUDE);
        LatLng latLng2 = new LatLng(lat2,lng2);
        RescueAction rescueAction2 = RescueAction.builder()
                .id(0L)
                .rescueActionType(RescueActionType.ACCIDENT)
                .rescueTeams(Collections.singletonList(rescueTeamRepository.findFirstByRescueTeamStatus(RescueTeamStatus.TRANSPORT).getId()))
                .time(LocalDateTime.now())
                .numberOfVictims(1L)
                .coordinates(latLng2)
                .injuries("Skręcenie stawu skokowego")
                .address("Kielce, Warszawska 34")
                .active(true)
                .build();

        rescueActionRepository.save(rescueAction2);
    }
}
