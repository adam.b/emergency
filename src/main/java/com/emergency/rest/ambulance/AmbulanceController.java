package com.emergency.rest.ambulance;

import com.emergency.domain.ambulance.AmbulanceType;
import com.emergency.service.AmbulanceService;
import com.emergency.service.ambulance.dto.AmbulanceDTO;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class AmbulanceController {

    @Autowired
    private AmbulanceService ambulanceService;

    @GetMapping(value = "/addAmbulance")
    public String addAmbulance(Model model) {
        List<String> ambulanceTypes = Arrays.stream(AmbulanceType.values())
                .map(AmbulanceType::getAmbulanceType)
                .collect(Collectors.toList());

        model.addAttribute("ambulanceTypes", ambulanceTypes);
        model.addAttribute("ambulanceDTO", new AmbulanceDTO());
        return "addAmbulance";
    }

    @PostMapping(value = "/addAmbulance")
    public String addAmbulance(@ModelAttribute("ambulanceDTO") AmbulanceDTO ambulanceDTO) {
        ambulanceService.createAmbulance(ambulanceDTO);
        System.out.println("Dodano pojazd");
        return "addAmbulance";
    }
}
