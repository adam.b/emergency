package com.emergency.rest.map;

import com.emergency.domain.rescueAction.RescueAction;
import com.emergency.domain.rescueTeam.RescueTeam;
import com.emergency.repository.ambulance.AmbulanceRepository;
import com.emergency.repository.rescueTeam.RescueTeamRepository;
import com.emergency.service.rescueAction.RescueActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MapController {

    @Autowired
    private RescueActionService rescueActionService;

    @Autowired
    private RescueTeamRepository rescueTeamRepository;

    @Autowired
    private AmbulanceRepository ambulanceRepository;

    @GetMapping(value = "/map")
    public String addMarkers(Model model) {
        List<List<String>> lists = new ArrayList<>();
        List<RescueTeam> rescueTeams = rescueTeamRepository.findAllByDate(LocalDate.now());

        for (RescueTeam team : rescueTeams){
            List<String> list = new ArrayList<>();
            list.add(String.valueOf(team.getId()));
            list.add(String.valueOf(team.getCoordinates().getLat()));
            list.add(String.valueOf(team.getCoordinates().getLng()));
            list.add(team.getRescueTeamStatus().getStatus());
            list.add(ambulanceRepository.findById(team.getAmbulance()).get().getAmbulanceType().getAmbulanceType());
            lists.add(list);
        }

        model.addAttribute("ambulanceLocations", lists);
        return "map";
    }
}
