package com.emergency.rest;

import com.emergency.repository.ambulance.AmbulanceRepository;
import com.emergency.repository.rescueAction.RescueActionRepository;
import com.emergency.repository.user.UserRepository;
import com.emergency.rest.access.AccessControler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WebController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AmbulanceRepository ambulanceRepository;

    @Autowired
    private RescueActionRepository rescueActionRepository;

    @Autowired
    private AccessControler accessControler;

    @RequestMapping(value="/")
    public String home(){
        return "index";
    }

    @RequestMapping(value="/user")
    public String user(){
        return "user";
    }

    @RequestMapping(value="/admin")
    public String admin(){
        return "admin";
    }

    @RequestMapping(value="/hello")
    public String hello(){
        return "hello";
    }

    @RequestMapping(value="/login")
    public String login(){
        return "login";
    }

    @RequestMapping(value="/403")
    public String Error403(){
        return "403";
    }

    @RequestMapping(value="/register")
    public String register(){
        return "register";
    }

    @RequestMapping(value="/map")
    public String map(){
        return "map";
    }

    @RequestMapping(value="/addRescueAction")
    public String addRescueAction(){
        return "addRescueAction";
    }

    @RequestMapping(value="/addUser")
    public String addUser(){
        return "addUser";
    }

    @RequestMapping(value="/addAmbulance")
    public String addAmbulance(){
        return "addAmbulance";
    }


    @GetMapping(value = "/users")
    public String getUsers(Model model) {

        model.addAttribute("users", userRepository.findAll());
        return "users";
    }

    @GetMapping(value = "/ambulances")
    public String getAmbulances(Model model) {

        model.addAttribute("ambulances", ambulanceRepository.findAll());
        return "ambulances";
    }

    @GetMapping(value = "/rescueActions")
    public String getRescueActions(Model model) {

        model.addAttribute("rescueActions", rescueActionRepository.findAll());
        return "rescueActions";
    }





}
