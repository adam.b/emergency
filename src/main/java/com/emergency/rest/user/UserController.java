package com.emergency.rest.user;

import com.emergency.domain.user.UserRoleType;
import com.emergency.service.user.UserService;
import com.emergency.service.user.dto.LoginDTO;
import com.emergency.service.user.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/login")
    public String loginForm(Model model) {
        model.addAttribute("loginDTO", new LoginDTO());
        return "login";
    }


    @GetMapping(value = "/logoutsuccess")
    public String logoutSuccess() {
        return "logoutsuccess";
    }


    @GetMapping(value = "/addUser")
    public String addUser(Model model) {
        List<String> roleTypes = Arrays.stream(UserRoleType.values())
                .map(UserRoleType::getUserRoleType)
                .collect(Collectors.toList());

        model.addAttribute("roleTypes", roleTypes);
        model.addAttribute("userDTO", new UserDTO());
        return "addUser";
    }

    @PostMapping(value = "/addUser")
    public String addAmbulance(@ModelAttribute("userDTO") UserDTO userDTO) {
        userService.createUser(userDTO);
        System.out.println("Dodano użytkownika");
        return "addUser";
    }

}
