package com.emergency.rest.access;

import com.emergency.domain.user.User;
import com.emergency.repository.user.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@Log
@RestController
public class AccessControler {

    @Autowired
    private UserRepository userRepository;


    @GetMapping(value = "/rest/getAccessToken")
    public String getAccessToken(@RequestParam("username") String login, @RequestParam("password") String password) throws JsonProcessingException, IOException {

        String status;

        User user = userRepository.findByUsername(login);
        if (user != null) {
            if (BCrypt.checkpw(password, user.getPassword())) {
                status = "userOk";
            } else {
                status = "badCredentials";
            }
        } else {
            status = "badUser";
        }

        return status;
    }
}

