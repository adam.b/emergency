package com.emergency.rest.rescueAction;

import com.emergency.domain.rescueAction.RescueAction;
import com.emergency.domain.rescueAction.RescueActionType;
import com.emergency.domain.rescueTeam.RescueTeam;
import com.emergency.domain.rescueTeam.RescueTeamStatus;
import com.emergency.domain.user.User;
import com.emergency.repository.rescueAction.RescueActionRepository;
import com.emergency.repository.rescueTeam.RescueTeamRepository;
import com.emergency.repository.user.UserRepository;
import com.emergency.service.rescueAction.RescueActionService;
import com.emergency.service.rescueAction.dto.RescueActionDTO;
import com.emergency.service.rescueTeam.dto.RescueTeamDTO;
import com.emergency.service.rescueTeam.mapper.RescueTeamMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class RescueActionController {

    @Autowired
    private RescueActionService rescueActionService;

    @Autowired
    private RescueTeamRepository rescueTeamRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RescueActionRepository rescueActionRepository;

    @GetMapping(value = "/addRescueAction")
    public String addRescueAction(Model model) {
        List<String> actionTypes = Arrays.stream(RescueActionType.values())
                .map(RescueActionType::getRescueActionType)
                .collect(Collectors.toList());
        Collection<RescueTeam> rescueTeams = rescueTeamRepository.findAllByDateAndRescueTeamStatus(LocalDate.now(), RescueTeamStatus.READY);
        Collection<RescueTeamDTO> rescueTeamsDTO = new ArrayList<>();
        for(RescueTeam team: rescueTeams){
            rescueTeamsDTO.add(RescueTeamMapper.domainToDTO(team));
        }
        model.addAttribute("rescueTeams", rescueTeamsDTO);
        model.addAttribute("actionTypes", actionTypes);
        model.addAttribute("rescueActionDTO", new RescueActionDTO());
        return "addRescueAction";
    }


    @PostMapping(value = "/addRescueAction")
    public String addRescueAction(@ModelAttribute("rescueActionDTO") RescueActionDTO rescueActionDTO) {
        rescueActionService.createRescueAction(rescueActionDTO);
        System.out.println("Dodano zgłoszenie");
        return "redirect:/map";
    }


}
