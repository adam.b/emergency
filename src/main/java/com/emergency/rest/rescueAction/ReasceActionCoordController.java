package com.emergency.rest.rescueAction;


import com.emergency.domain.rescueAction.RescueAction;
import com.emergency.domain.rescueTeam.RescueTeam;
import com.emergency.domain.rescueTeam.RescueTeamStatus;
import com.emergency.domain.user.User;
import com.emergency.repository.rescueAction.RescueActionRepository;
import com.emergency.repository.rescueTeam.RescueTeamRepository;
import com.emergency.repository.user.UserRepository;
import com.emergency.service.rescueAction.RescueActionService;
import lombok.extern.java.Log;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Optional;

@Log
@RestController
public class ReasceActionCoordController {


    @Autowired
    private RescueActionService rescueActionService;

    @Autowired
    private RescueTeamRepository rescueTeamRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RescueActionRepository rescueActionRepository;


    @GetMapping(value = "/rest/getCoordinates")
    public String getCoordinates(@RequestParam("username") String username) {
        Double[] coords = new Double[2];
        RescueAction rescueAction = rescueActionService.getRescueActionByUsername(username);

        coords[0] = rescueAction.getCoordinates().getLat();
        coords[1] = rescueAction.getCoordinates().getLng();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("lat" , rescueAction.getCoordinates().getLat());
        jsonObject.put("lng" , rescueAction.getCoordinates().getLng());

        return jsonObject.toString();
    }

    @GetMapping(value = "/rest/getRescueActionDetails")
    public String getRescueActionDetails(@RequestParam("username") String username){

        RescueAction rescueAction = rescueActionService.getRescueActionByUsername(username);
        Optional<RescueTeam> rescueTeam = rescueTeamRepository.findById(rescueAction.getRescueTeams().get(0));

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", rescueAction.getRescueActionType().getRescueActionType());
        jsonObject.put("address", rescueAction.getAddress());
        jsonObject.put("numberOfVictims", rescueAction.getNumberOfVictims());
        jsonObject.put("injuries", rescueAction.getInjuries());
        jsonObject.put("status", rescueTeam.get().getRescueTeamStatus().getStatus());

        return jsonObject.toString();
    }

    @GetMapping(value = "/rest/setActionStatus")
    public String setActionStatus(@RequestParam("username") String username, @RequestParam("status") String status){

        RescueTeamStatus rescueTeamStatus = null;

        switch (status){
            case "GOTOWY DO AKCJI":{
                rescueTeamStatus = RescueTeamStatus.READY;
                break;
            }
            case "BRAK CZYNNOSCI":{
                rescueTeamStatus = RescueTeamStatus.NO_ACTION;
                break;
            }
            case "TRANSPORT":{
                rescueTeamStatus = RescueTeamStatus.TRANSPORT;
                break;
            }
            case "W DRODZE":{
                rescueTeamStatus = RescueTeamStatus.ON_THE_WAY;
                break;
            }
            case "PODEJMOWANIE CZYNNOSCI":{
                rescueTeamStatus = RescueTeamStatus.ON_ACTION;
                break;
            }

        }
        User user = userRepository.findByUsername(username);
        RescueTeam rescueTeam = rescueTeamRepository.findFirstByRescuersAndDate(user.getId(), LocalDate.now());
        rescueTeam.setRescueTeamStatus(rescueTeamStatus);
        rescueTeamRepository.save(rescueTeam);
        return status;
    }
}
