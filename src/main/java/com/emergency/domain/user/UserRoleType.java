package com.emergency.domain.user;

public enum UserRoleType {

    ROLE_DOCTOR(Roles.ROLE_DOCTOR), ROLE_DISPATCHER(Roles.ROLE_DISPATCHER), ROLE_RESCUER(Roles.ROLE_RESCUER), ROLE_ADMIN(Roles.ROLE_ADMIN);

    String userRoleType;

    UserRoleType(String userRoleType) {
        this.userRoleType = userRoleType;
    }

    public String getUserRoleType() {
        return userRoleType;
    }

    public class Roles {
        static final String ROLE_DOCTOR = "ROLE_DOCTOR";
        static final String ROLE_DISPATCHER = "ROLE_DISPATCHER";
        static final String ROLE_RESCUER = "ROLE_RESCUER";
        static final String ROLE_ADMIN = "ROLE_ADMIN";

    }


}

