package com.emergency.domain.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@Builder
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String username;


    private String password;

    private String name;

    private String surname;

    private String email;

    @OneToOne
    private UserRole role;

    private Boolean active;

    public User(){}



}
