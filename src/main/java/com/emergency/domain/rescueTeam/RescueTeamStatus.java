package com.emergency.domain.rescueTeam;

public enum RescueTeamStatus {

    ON_ACTION("PODEJMOWANIE CZYNNOSCI"), ON_THE_WAY("W DRODZE"), READY("GOTOWY DO AKCJI"), TRANSPORT("TRANSPORT"), NO_ACTION("BRAK CZYNNOSCI");

    private String status;

    RescueTeamStatus(String status) {
        this.status = status;
    }

    public String getStatus(){
        return status;
    }
}
