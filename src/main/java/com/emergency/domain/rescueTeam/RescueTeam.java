package com.emergency.domain.rescueTeam;

import com.emergency.domain.ambulance.Ambulance;
import com.emergency.domain.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.primefaces.model.map.LatLng;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class RescueTeam {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @ElementCollection(targetClass=Long.class)
    private List<Long> rescuers;

    private LocalDate date;

    private Long ambulance;

    private LatLng coordinates;

    private RescueTeamStatus rescueTeamStatus;
}
