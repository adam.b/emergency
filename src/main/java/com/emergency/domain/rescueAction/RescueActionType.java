package com.emergency.domain.rescueAction;

import java.util.stream.Stream;

public enum RescueActionType {

    ACCIDENT("WYPADEK"), TRANSPORT("TRANSPORT");

    public String type;

    RescueActionType(String type) {
        this.type = type;
    }

    public String getRescueActionType(){
        return this.type;
    }


    public static RescueActionType getInstance(String name){
        return Stream.of(values())
                .filter(actionType -> actionType.type.equals(name))
                .findFirst()
                .orElse(null);
    }
}
