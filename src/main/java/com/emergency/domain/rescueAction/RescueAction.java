package com.emergency.domain.rescueAction;

import com.emergency.domain.address.Address;
import com.emergency.domain.rescueTeam.RescueTeam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.primefaces.model.map.LatLng;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class RescueAction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private RescueActionType rescueActionType;

    private LocalDateTime time;

    private Long numberOfVictims;

    private String injuries;

    @Column
    @ElementCollection(targetClass=Long.class)
    private List<Long> rescueTeams;

    private String address;

    private LatLng coordinates;

    private Boolean active;

}
