package com.emergency.domain.ambulance;


import com.emergency.domain.rescueAction.RescueActionType;

import java.util.stream.Stream;

public enum AmbulanceType {

    SPECIALIST("SPECIALIST"), BASIC("BASIC"), TRANSPORT("TRANSPORT");

    private String type;

    AmbulanceType(String type) {
        this.type = type;
    }

    public String getAmbulanceType(){
        return type;
    }


    public static AmbulanceType getInstance(String name){
        return Stream.of(values())
                .filter(ambulanceType -> ambulanceType.type.equals(name))
                .findFirst()
                .orElse(null);
    }
}
