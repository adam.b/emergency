package com.emergency.repository.rescueTeam;

import com.emergency.domain.rescueTeam.RescueTeam;
import com.emergency.domain.rescueTeam.RescueTeamStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface RescueTeamRepository extends JpaRepository<RescueTeam, Long> {

    RescueTeam findFirstByRescuersAndDate(Long rescurerId, LocalDate date);
    RescueTeam findFirstByRescueTeamStatus(RescueTeamStatus rescueTeamStatus);
    List<RescueTeam> findAllByDate(LocalDate localDate);
    List<RescueTeam> findAllByDateAndRescueTeamStatus(LocalDate localDate, RescueTeamStatus rescueTeamStatus);
}
