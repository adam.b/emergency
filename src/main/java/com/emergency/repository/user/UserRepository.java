package com.emergency.repository.user;

import com.emergency.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;


public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
