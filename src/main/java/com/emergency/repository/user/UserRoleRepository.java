package com.emergency.repository.user;

import com.emergency.domain.user.UserRole;
import com.emergency.domain.user.UserRoleType;
import org.springframework.data.repository.CrudRepository;

public interface UserRoleRepository extends CrudRepository<UserRole, Long> {

    UserRole findDistinctByUserRoleType(UserRoleType userRoleType);
}
