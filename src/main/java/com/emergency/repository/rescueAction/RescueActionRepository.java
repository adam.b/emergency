package com.emergency.repository.rescueAction;

import com.emergency.domain.rescueAction.RescueAction;
import com.emergency.domain.rescueTeam.RescueTeam;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RescueActionRepository extends JpaRepository<RescueAction, Long> {

    RescueAction findByRescueTeamsAndActiveTrue(Long rescueTeamId);
    List<RescueAction> findAllByActiveTrue();
}
