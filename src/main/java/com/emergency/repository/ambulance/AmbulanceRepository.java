package com.emergency.repository.ambulance;

import com.emergency.domain.ambulance.Ambulance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AmbulanceRepository extends JpaRepository<Ambulance, Long> {
    Ambulance findByLicensePlate(String licensePlate);
}
